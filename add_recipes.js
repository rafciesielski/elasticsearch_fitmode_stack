console.log('add_recipes')

var elasticsearch = require('elasticsearch');

var client = new elasticsearch.Client({
  host: 'http://elasticsearch-rci.rhcloud.com',
  log: 'trace'
});

client.create({
  index: 'fitmode_recipes',
  type: 'recipe',
  id: 'Teriyaki_Chicken',
  body: {
    name: 'Teriyaki Chicken',
    cuisine: 'Asian',
    ingredients: ['4 boneless chicken breast halves',  '1/3 cup Soy Sauce',  '1/3 cup Vegetable Oil'],
    totaltime: 120,
    activetime: 30,
    calories: 528
  }
}, function (err, response) {
    // ...
});

client.create({
  index: 'fitmode_recipes',
  type: 'recipe',
  id: 'Cambodian_Grilled_Chicken',
  body: {
    name: 'Cambodian Grilled Chicken',
    cuisine: 'Asian',
    ingredients: ['1 ea fryer chicken', '1 ea green onion', '5 cloves garlic'],
    totaltime: 240,
    activetime: 45,
    calories: 1018
  }
}, function (err, response) {
    // ...
});

client.create({
  index: 'fitmode_recipes',
  type: 'recipe',
  id: 'Crispy_Fried_Fish_Asian_Style',
  body: {
    name: 'Crispy Fried Fish Asian Style',
    cuisine: 'Asian',
    ingredients: ['1 whole fish', '3 cloves garlic', '100 g ginger'],
    totaltime: 60,
    activetime: 45,
    calories: 500
  }
}, function (err, response) {
    // ...
});