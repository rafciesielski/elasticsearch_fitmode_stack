module.exports = function(grunt){
	
	grunt.loadNpmTasks('grunt-execute');

	grunt.initConfig({
		execute: {
	        find_recipe: {
	            src: ['find_recipe.js']
	        },
	        add_recipes: {
	            src: ['add_recipes.js']
	        },
	        del_recipes: {
	            src: ['del_recipes.js']
	        }	        
	    }
	})

	grunt.registerTask('find', ['execute:find_recipe']);
	grunt.registerTask('add', ['execute:add_recipes']);
	grunt.registerTask('del', ['execute:del_recipes']);
}	