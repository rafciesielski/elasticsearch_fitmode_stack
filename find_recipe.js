console.log('find_recipe')

var elasticsearch = require('elasticsearch');

var client = new elasticsearch.Client({
  host: 'http://elasticsearch-rci.rhcloud.com',
  log: 'trace'
});

client.search({
  index: 'fitmode_recipes',
  type: 'recipe',
  body: {
    query: {
      filtered: {
        query: {
          bool: {
            must: [ 
              { match: { cuisine: 'Asian' }},
              { match: { ingredients: 'chicken' }}              
            ]
          }
        },
        filter: {
          range: {
            calories: {
              lte: 1000
            }
          }
        }
      } 
    }
  }
}).then(function (resp) {
    var hits = resp.hits.hits;
}, function (err) {
    console.trace(err.message);
});