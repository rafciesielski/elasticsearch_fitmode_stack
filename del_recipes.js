console.log('del_recipes')

var elasticsearch = require('elasticsearch');

var client = new elasticsearch.Client({
  host: 'http://elasticsearch-rci.rhcloud.com',
  log: 'trace'
});


client.indices.delete({
  index: 'fitmode_recipes'
}, function (error, response) {
  // ...
});